'use strict';

const gulp = require('gulp');
const browserSync = require('./browser-sync');
const { reloadConfig } = require('./utils');

// Watch task to track file changes and refresh only affected files
gulp.task('watch', gulp.series(reloadConfig, 'compile-html', 'browserSyncServe', function(done) {
  const config = global.config || require('./config.js');

  // Watch SCSS files with notification
  gulp.watch(config.paths.src.scssFilesWatched, function(cb) {
    // Notify that SCSS is compiling
    browserSync.notify('Compiling SCSS... Please wait.');

    gulp.series('compile-scss')(function() {
      // Reload only CSS after SCSS files have been compiled
      browserSync.reload('*.css');
      // Notify that SCSS compilation is done
      browserSync.notify('SCSS Compiled!', 20000); // Show "SCSS Compiled!" for 2 seconds

      gulp.series('compile-plugins-css')(function() {
        browserSync.reload('*.css');
      });
      cb();
    });
  });

  // Watch JS files with notification
  gulp.watch(config.paths.src.jsDir + '/**/*.js', function(cb) {
    // Notify that JS is compiling
    browserSync.notify('Compiling JS... Please wait.');

    gulp.series('compile-js')(function() {
      // Reload only JS after JS files have been compiled
      browserSync.reload('*.js');
      // Notify that JS compilation is done
      browserSync.notify('JS Compiled!', 2000); // Show "JS Compiled!" for 2 seconds
      cb();
    });
  });

  // Watch HTML files with notification
  gulp.watch(config.paths.src.htmlDir + '/**/*.html', function(cb, file) {
    // Notify that HTML is compiling
    browserSync.notify('Compiling HTML... Please wait.', 10000);

    gulp.series(reloadConfig, 'compile-html')(function() {
      // Reload the exact HTML file that changed
      browserSync.reload(file);
      // Notify that HTML compilation is done
      browserSync.notify('HTML Compiled!', 2000); // Show "HTML Compiled!" for 2 seconds
      cb();
    });
  });

  // Watch assets files with notification
  gulp.watch([config.paths.src.assetsDir + '/**/*'], function(cb) {
    // Notify that assets are being copied
    browserSync.notify('Copying Assets... Please wait.');

    gulp.series('copy-assets', 'browserSyncReload')(function() {
      // Notify that assets have been copied
      browserSync.notify('Assets Copied!', 2000); // Show "Assets Copied!" for 2 seconds
      cb();
    });
  });

  // Watch config files and trigger reload with notification
  gulp.watch([
    './src/config.default.js',
    './config.custom.js',
    './projects/**/config.*.js'
  ], gulp.series(reloadConfig, function(cb) {
    // Notify that configuration is being reloaded
    browserSync.notify('Reloading Config... Please wait.', 100000);

    gulp.series('compile-html', 'compile-scss', 'compile-js', 'browserSyncReload')(function() {
      // Notify that config reloading is done
      browserSync.notify('Config Reloaded!', 2000); // Show "Config Reloaded!" for 2 seconds
      cb();
    });
  }));

  done();
}));
