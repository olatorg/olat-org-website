'use strict';

const gulp = require('gulp');
const { logTaskError, logger } = require('./utils');

// Gulp task to compress images using async/await
gulp.task('compress-imgs', async function() {
  const config = global.config || require('./config.js');
  let imageCount = 0;  // Variable to count the number of images

  try {
    // Dynamically import gulp-imagemin and individual plugins
    const imagemin = (await import('gulp-imagemin')).default;
    const imageminGifsicle = (await import('imagemin-gifsicle')).default;
    const imageminMozjpeg = (await import('imagemin-mozjpeg')).default;
    const imageminOptipng = (await import('imagemin-optipng')).default;
    const imageminSvgo = (await import('imagemin-svgo')).default;

    await gulp.src(config.paths.dist.imgDir + '/**')
      .pipe(imagemin([
        imageminGifsicle({ interlaced: true }),
        imageminMozjpeg({ quality: 75, progressive: true }),
        imageminOptipng({ optimizationLevel: 5 }),
        imageminSvgo({
          plugins: [
            {
              name: 'preset-default',
              params: {
                overrides: {
                  removeViewBox: false,  // Keep the viewBox attribute
                  // You can add more configurations as necessary
                }
              }
            }
          ]
        })
      ], {
        verbose: true
      }))
      .on('data', function(file) {
        if (imageCount === 0) {
          logger.info(`Images within ${config.paths.dist.imgDir} will be compressed and resaved. NOTE: This task can take a while if you have a lot of images.`);
        }

        imageCount++;  // Increment the image count
        logger.debug(`Image compressed: ${file.path}`);
      })
      .pipe(gulp.dest(config.paths.dist.imgDir))
      .on('end', function() {
        logger.info(`Total images processed: ${imageCount}`);
      });

  } catch (err) {
    logTaskError('compress-imgs')(err);  // Log any errors
  }
});
