'use strict';

const gulp = require('gulp');
const twig = require('gulp-twig');
const rename = require('gulp-rename');
const data = require('gulp-data');
const path = require('path');
const fs = require('fs');
const { logger } = require('./utils');
const buffer = require('gulp-buffer');
const prettify = require('gulp-jsbeautifier');
const replace = require('gulp-replace');

// Helper function to resolve settings with inheritance and wildcard support
function resolvePageSettings(fileName, config) {
  let matchedSettings = {};

  for (const key in config.pageSettings) {
    const regex = new RegExp(`^${key.replace(/\*/g, '.*')}$`);
    if (regex.test(fileName)) {
      matchedSettings = { ...matchedSettings, ...config.pageSettings[key] };
    }
  }

  if (matchedSettings.inheritFrom) {
    const inheritedSettings = config.pageSettings[matchedSettings.inheritFrom] || {};
    matchedSettings = { ...inheritedSettings, ...matchedSettings };
    delete matchedSettings.inheritFrom;
  }

  return matchedSettings;
}

// Function to extract JSON-like front matter from the HTML files
function extractPageSettings(fileContent, fileName) {
  const frontMatterPattern = /<!--\s*({[\s\S]*?})\s*-->/;
  const match = fileContent.match(frontMatterPattern);
  if (match && match[1]) {
    try {
      return {
        settings: JSON.parse(match[1]),
        cleanedContent: fileContent.replace(frontMatterPattern, '') // Remove front matter from content
      };
    } catch (err) {
      console.error(`Error parsing JSON front matter in ${fileName}: ${err.message}`);
    }
  }
  return { settings: {}, cleanedContent: fileContent };
}

gulp.task('compile-html', function() {
  const config = global.config || require('./config.js');

  logger.info(`HTML files within ${config.paths.src.htmlDir} will be compiled and copied to ${config.paths.dist.htmlDir}`);

  return gulp.src([
    `${config.paths.src.htmlDir}/**/*.html`, // Include all HTML files in htmlDir
    `!${config.paths.src.htmlPartialsDir}/**/*.html` // Exclude HTML files in htmlPartialsDir
  ])
    .pipe(data((file) => {
      const fileName = path.basename(file.path, '.html');
      const fileContent = fs.readFileSync(file.path, 'utf8');

      // Extract front matter settings from the HTML file
      const { settings: pageSettings, cleanedContent } = extractPageSettings(fileContent, fileName);
      
      // Resolve page-specific settings
      const resolvedSettings = resolvePageSettings(fileName, config);

      // Extract currentPageKey from pageSettings
      const currentPageKey = pageSettings.pageKey || fileName;

      // Merge global settings, resolved settings, page-specific settings, and paths
      const mergedVars = { 
        ...config.settings, 
        ...resolvedSettings, 
        ...pageSettings, 
        currentPageKey, // Pass currentPageKey to the mergedVars
        ...config.paths 
      };

      file.contents = Buffer.from(cleanedContent); // Use cleaned content

      // Attach mergedVars to file.data
      file.data = mergedVars;
      return mergedVars;
    }))
    .pipe(buffer()) // Ensure content is passed as buffer before twig() is called
    .pipe(twig({
      base: config.paths.src.htmlDir,
      extname: '.html',
      errorLogToConsole: true,
      useFileContents: true,
      namespaces: {
        'partials': config.paths.src.htmlPartialsDir // Define the namespace for partials
      },
      filters: [
        {
          name: "classMerge",
          func: function (...args) {
            return args.flat().filter(Boolean).join(' ');
          }
        }
      ],
      functions: [
        {
          name: "pageIsActive",
          func: function (pageKey, currentPageKey, pageActiveTrail = []) {
            return pageKey === currentPageKey || pageActiveTrail.includes(pageKey);
          }
        }
      ]
    }))
    // Remove specific comments
    .pipe(replace(/<!--\s*Twig include: @see.*?-->/g, '')) // This line removes the comments
    .pipe(prettify({ indent_size: 2, max_preserve_newlines: 1 }))
    .pipe(rename({ extname: '.html' }))
    .pipe(gulp.dest(config.paths.dist.base));
});
