'use strict';

const gulp = require('gulp');
const fs = require('fs-extra');
const path = require('path');
const prompts = require('prompts');
const beautify = require('js-beautify').js; // For prettifying the config file
const { logger } = require('./utils');

// Gulp task to create a new project
gulp.task('create-project', async function (done) {
  const argv = require('yargs')
    .option('name', {
      describe: 'Machine name for the project',
      demandOption: true,
      type: 'string'
    })
    .option('template', {
      describe: 'Template to create project from (coming soon)',
      demandOption: false,
      type: 'string'
    })
    .help()
    .argv;

  const projectName = argv.name;
  const projectTemplate = argv.template || 'full';
  const projectDir = path.join(__dirname, '../projects', projectName);
  const srcDir = path.join(__dirname, '../src');

  const projectSrcDir = path.join(projectDir, 'src');
  const projectScssDir = path.join(projectSrcDir, 'scss');
  const projectJsDir = path.join(projectSrcDir, 'js');
  const projectHtmlDir = path.join(projectSrcDir, 'html');
  const projectAssetsDir = path.join(projectSrcDir, 'assets');

  const defaultConfigFile = path.join(srcDir, 'config.default.js');
  const newConfigFile = path.join(projectDir, `config.${projectName}.js`); // Save in the project directory

  // Check if the project directory already exists
  if (fs.existsSync(projectDir)) {
    const response = await prompts({
      type: 'confirm',
      name: 'overwrite',
      message: `The project directory "${projectName}" already exists. Do you want to overwrite it?`,
      initial: false
    });

    if (!response.overwrite) {
      const newAnswer = await prompts({
        type: 'text',
        name: 'newName',
        message: 'Please provide a new name for the project:',
        validate: input => (input.trim() !== '' ? true : 'Project name cannot be empty!')
      });

      // Restart with the new name
      argv.name = newAnswer.newName;
      return gulp.task('create-project')(done);
    } else {
      logger.info(`Overwriting project directory: ${projectDir}`);
      fs.removeSync(projectDir); // Clear the existing directory before proceeding
    }
  }

  // 1) Create project folder in ./projects/NAME
  fs.ensureDirSync(projectDir);
  logger.info(`Created project directory: ${projectDir}`);

  // 2) Copy specific SCSS files
  fs.ensureDirSync(projectScssDir);
  const scssFilesToCopy = [
    '_custom.maps.scss',
    '_custom.style.scss',
    '_custom.variables.scss',
    'theme.style.scss',
  ];

  scssFilesToCopy.forEach((file) => {
    const srcFilePath = path.join(srcDir, 'scss', file);
    const destFilePath = path.join(projectScssDir, file);
    fs.copySync(srcFilePath, destFilePath);
    logger.info(`Copied SCSS file: ${file}`);
  });

  // 3) Copy JS files
  fs.ensureDirSync(projectJsDir);
  fs.copySync(path.join(srcDir, 'js', 'custom.script.js'), path.join(projectJsDir, 'custom.script.js'));
  fs.copySync(path.join(srcDir, 'js', 'README.txt'), path.join(projectJsDir, 'README.txt'));

  // 4) Copy HTML & assets
  fs.copySync(path.join(srcDir, 'assets'), projectAssetsDir);
  fs.copySync(path.join(srcDir, 'html'), projectHtmlDir);
  logger.info(`Copied ./src to ${projectSrcDir}`);

  // 5) Read the default config file and load the object
  let defaultConfigContent = fs.readFileSync(defaultConfigFile, 'utf8');
  const defaultConfig = require(defaultConfigFile);

  // Ensure scssIncludePaths exists and add the new path
  if (!Array.isArray(defaultConfig.paths.src.scssIncludePaths)) {
    defaultConfig.paths.src.scssIncludePaths = [];
  }
  defaultConfig.paths.src.scssIncludePaths.push(`./projects/${projectName}/src/scss`);

  // Ensure pluginsScssFiles exists and add the new path
  if (!Array.isArray(defaultConfig.paths.src.pluginsScssFiles)) {
    defaultConfig.paths.src.pluginsScssFiles = [];
  }
  if (!defaultConfig.paths.src.pluginsScssFiles.includes('./src/scss/plugin/*.scss')) {
    defaultConfig.paths.src.pluginsScssFiles.unshift('./src/scss/plugin/*.scss');
  }

  // Ensure scssFilesWatched exists and add the parent directories
  if (!Array.isArray(defaultConfig.paths.src.scssFilesWatched)) {
    defaultConfig.paths.src.scssFilesWatched = [];
  }
  const scssFilesWatched = defaultConfig.paths.src.scssFilesWatched.filter(file => {
    return file !== './src/scss/*.scss';
  });
  scssFilesWatched.unshift(`./projects/${projectName}/src/scss/*.scss`, `./projects/${projectName}/src/scss/**/*.scss`);

  // Preserve JSSourceFiles as is
  const jsSourceFiles = defaultConfig.paths.src.JSSourceFiles;

  // Replace the object string in the config file but maintain other parts
  defaultConfigContent = defaultConfigContent
    // Replace only src and dist paths
    .replace(/\.\/src/g, `./projects/${projectName}/src`)
    .replace(/\.\/dist/g, `./projects/${projectName}/dist`)
    // Handle scssIncludePaths, ensuring the './src/scss' path remains intact
    .replace(
      /scssIncludePaths: \[([\s\S]*?)\]/,
      () => `scssIncludePaths: ${JSON.stringify(defaultConfig.paths.src.scssIncludePaths, null, 2)}`
    )
    // Ensure JSSourceFiles remain unchanged
    .replace(/JSSourceFiles: \[([\s\S]*?)\]/, () => `JSSourceFiles: ${JSON.stringify(jsSourceFiles, null, 2)}`)
    // Handle pluginsScssFiles
    .replace(
      /pluginsScssFiles: \[([\s\S]*?)\]/,
      () => `pluginsScssFiles: ${JSON.stringify(defaultConfig.paths.src.pluginsScssFiles, null, 2)}`
    )    
    // Handle scssFilesWatched
    .replace(
      /scssFilesWatched: \[([\s\S]*?)\]/,
      () => `scssFilesWatched: ${JSON.stringify(scssFilesWatched, null, 2)}`
    );

  // 6) Prettify the config file content before saving
  const prettifiedConfig = beautify(defaultConfigContent, {
    indent_size: 2,
    space_in_empty_paren: true,
    preserve_newlines: true,
    max_preserve_newlines: 2
  });

  // 7) Write the modified config to the new config file
  fs.writeFileSync(newConfigFile, prettifiedConfig);
  logger.info(`Created prettified config file: ${newConfigFile}`);

  // 8) Show example commands for the user
  console.log('\nProject created successfully!\n');
  console.log(`The project directory is: ${path.relative('./', projectDir)}`);
  console.log(`The project config can be found at: ${path.relative('./', newConfigFile)}\n`);
  console.log('You can now use the following commands to build your project:');
  console.log(`$ yarn gulp --project=${projectName}`);
  console.log(`$ yarn gulp build --project=${projectName}\n`);

  done();
});
