'use strict';

const fs = require('fs');
const path = require('path');
const yargs = require('yargs');

// Get the project name from the CLI arguments
const argv = yargs
  .option('project', {
    describe: 'The project name to load custom configuration',
    type: 'string'
  })
  .help()
  .argv;

// Load the default configuration
let config = require('../src/config.default.js');

// Determine if a project-based config should be loaded
if (argv.project) {
  const projectConfigPath = path.join(__dirname, '../projects', argv.project, `config.${argv.project}.js`);

  // Check if the project configuration file exists
  if (fs.existsSync(projectConfigPath)) {
    console.log('Project config loaded from: ' + projectConfigPath);
    const projectConfig = require(projectConfigPath);
    config = {
      ...config,
      ...projectConfig
    };
  } else {
    console.error(`Project config file not found for project: ${argv.project}`);
  }
} else {
  // Load the custom config (if exists)
  const customConfigPath = path.join(__dirname, '../config.custom.js');
  if (fs.existsSync(customConfigPath)) {
    console.log('Custom config loaded from: ' + customConfigPath);
    const customConfig = require(customConfigPath);
    config = {
      ...config,
      ...customConfig
    };
  }
}

module.exports = config;
