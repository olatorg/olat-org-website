'use strict';

const gulp = require('gulp');
const fs = require('fs-extra');
const path = require('path');
const cheerio = require('cheerio');
const map = require('map-stream');
const { logger } = require('./utils');

// Attributes to look for in the HTML files
const attributes = [
  "data-backstretch-imgs",
  "data-bg-img",
  "data-src",
  "data-bg-video",
  "src",
  "data-cbp-src",
  "data-thumb",
  "data-lazyload",
  "href"
];

// Function to validate the path of an asset
function isValidPath(assetPath) {
  return assetPath !== 'undefined' &&
    !/^(https?:)?\/\//.test(assetPath) &&
    assetPath.indexOf('assets/') !== -1;
}

// Gulp task to map assets used in HTML files
gulp.task('assets-map', function () {
  const config = global.config || require('./config.js');
  const htmlFiles = [
    path.join(config.paths.dist.htmlDir, '*.html'), // Top-level HTML files in dist.htmlDir
    path.join(config.paths.dist.htmlDir, 'includes/**/*.html') // HTML files in the includes directory
  ];
  const assetsMapDir = path.join(config.paths.dist.assetsDir);
  const assetsMapFile = path.join(assetsMapDir, 'assets-map.json');

  const assetsMap = {};

  // Ensure the assetsMapDir exists
  fs.ensureDirSync(assetsMapDir);

  return gulp.src(htmlFiles)
    .pipe(map(function (file, done) {
      const filePath = file.relative;
      const fileDir = path.dirname(filePath);
      const pageAssets = [];

      // Load the HTML into Cheerio
      const $ = cheerio.load(file.contents.toString());

      // Iterate over each attribute to find asset paths
      attributes.forEach(attr => {
        $(`[${attr}]`).each((index, element) => {
          const assetPath = $(element).attr(attr);
          if (isValidPath(assetPath)) {
            const multi = assetPath.split(',');
            multi.forEach(ml => {
              if (isValidPath(ml)) {
                pageAssets.push(ml.trim());
              }
            });
          }
        });
      });

      // Save the assets for this specific HTML file
      if (!assetsMap[filePath]) {
        assetsMap[filePath] = [];
      }
      assetsMap[filePath] = [...new Set([...assetsMap[filePath], ...pageAssets])]; // Remove duplicates

      done(null, file);
    }))
    .on('end', function () {
      // Write the assets map to the JSON file
      fs.writeJsonSync(assetsMapFile, assetsMap, { spaces: 2 });
      logger.info(`Assets map saved to ${assetsMapFile}`);
    })
    .pipe(gulp.dest(function (file) {
      return file.base;
    }));
});
