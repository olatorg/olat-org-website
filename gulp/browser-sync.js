// /gulp/browserSync.js

'use strict';

const gulp = require('gulp');
const browserSync = require('browser-sync').create(); // Create the browserSync instance

// BrowserSync serve task
gulp.task('browserSyncServe', function(done) {
  const config = global.config || require('./config.js');

  browserSync.init({
    server: {
      baseDir: config.paths.dist.base
    },
    host: 'localhost', // Force BrowserSync to use IPv4
    port: 3003
  });
  done(); // Signal async completion
});

// Reload the browser
gulp.task('browserSyncReload', function(done) {
  browserSync.reload();
  done(); // Signal async completion
});

module.exports = browserSync; // Export the shared instance
