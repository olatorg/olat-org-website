'use strict';

const gulp = require('gulp');
const prettify = require('gulp-jsbeautifier');
const { logger } = require('./utils');

gulp.task('prettify', function() {
  const config = global.config || require('./config.js');

  logger.info(`Optional: Prettify your .css, .html and .js files.`);

  return gulp.src([
      config.paths.dist.cssDir + '/**/*.css',  // Find all .css files
      config.paths.dist.jsDir + '/**/*.js',    // Find all .js files
      '!' + config.paths.dist.cssDir + '/**/*.min.css', // Exclude .min.css files
      '!' + config.paths.dist.jsDir + '/**/*.min.js'    // Exclude .min.js files
    ])
    .on('data', function(file) {
      logger.debug(`Prettify applied to: ${file.relative}`); // Log each file found
    })
    .pipe(prettify({ 
      indent_size: 2,
      wrap_line_length: 180,
    }))
    .pipe(gulp.dest(function(file) {
      return file.base;
    }));
});

