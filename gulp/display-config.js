'use strict';

const gulp = require('gulp');
const config = require('./config');
const { logger } = require('./utils');

// Gulp task to display the current configuration
gulp.task('display-config', async function() {
  logger.info('Displaying the current configuration used by Gulp:');
  console.log(JSON.stringify(config, null, 2)); // Pretty print the config object
});
