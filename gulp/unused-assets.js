'use strict';

const gulp = require('gulp');
const fs = require('fs-extra');
const path = require('path');

// Task to remove unused assets
gulp.task('unused-assets', gulp.series('assets-map', function(done) {  // Run assets-map before unused-assets
  const config = global.config || require('./config.js');
  const assetsMapFile = path.join(config.paths.dist.assetsDir, 'assets-map.json');

  if (fs.existsSync(assetsMapFile)) {
    const assetsMap = fs.readJsonSync(assetsMapFile);
    const usedAssets = new Set();

    // Collect all used assets from the assets map
    Object.values(assetsMap).forEach(assets => {
      assets.forEach(asset => {
        const normalizedAsset = path.normalize(asset);
        if (normalizedAsset.startsWith(path.relative(config.paths.dist.base, config.paths.dist.assetsDir))) {
          usedAssets.add(normalizedAsset);
        }
      });
    });

    // Function to get all files in a directory recursively
    const getAllFiles = (dirPath, arrayOfFiles) => {
      const files = fs.readdirSync(dirPath);

      arrayOfFiles = arrayOfFiles || [];

      files.forEach(file => {
        if (fs.statSync(path.join(dirPath, file)).isDirectory()) {
          arrayOfFiles = getAllFiles(path.join(dirPath, file), arrayOfFiles);
        } else {
          arrayOfFiles.push(path.join(dirPath, file));
        }
      });

      return arrayOfFiles;
    };

    // Get all files in the img and videos directories
    const imgDir = path.join(config.paths.dist.assetsDir, 'img');
    const videosDir = path.join(config.paths.dist.assetsDir, 'videos');
    const cssDir = config.paths.dist.cssDir;
    const jsDir = config.paths.dist.jsDir;

    const allAssets = [
      ...getAllFiles(imgDir),
      ...getAllFiles(videosDir),
      ...getAllFiles(cssDir),
      ...getAllFiles(jsDir)
    ];

    // Check each file to see if it's in the usedAssets set, delete if not
    allAssets.forEach(assetPath => {
      // Get the relative path from the assets directory
      const relativePath = path.relative(config.paths.dist.base, assetPath);

      // Normalize the path to ensure proper comparison
      const normalizedPath = path.normalize(relativePath);

      // Check if this file is used; if not, remove it
      if (!usedAssets.has(normalizedPath)) {
        fs.removeSync(assetPath);
        console.log(`Removed unused asset: ${assetPath}`);
      }
    });
  } else {
    //console.log(`Assets map file not found at ${assetsMapFile}`);
  }

  done();
}));
