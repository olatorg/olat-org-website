'use strict';

const gulp = require('gulp');
const { logger } = require('./utils');
const sass = require('gulp-sass')(require('sass'));
const cssmin = require('gulp-cssmin');
const rename = require('gulp-rename');
const prettify = require('gulp-jsbeautifier');
const path = require('path');

// Function to dynamically load autoprefixer
async function loadAutoprefixer() {
  const module = await import('gulp-autoprefixer');
  return module.default;
}

gulp.task('compile-plugins-css', function(done) {
  const config = global.config || require('./config.js');

  loadAutoprefixer().then(autoprefixer => {
    logger.info(`SCSS files within ${config.paths.src.scssDir + '/plugins/*.scss'} will be compiled and copied to ${config.paths.dist.pluginsCssDir}`);

    // Include paths
    let includePaths = [
      path.resolve(__dirname, '../node_modules'),
      config.paths.src.base,
    ];
    
    // Merge additional include paths if paths.src.scssIncludePaths is set
    if (Array.isArray(config.paths.src.scssIncludePaths)) {
      includePaths = includePaths.concat(config.paths.src.scssIncludePaths);
      logger.info(`Merging custom SCSS include paths: ${includePaths.join(', ')}`);
    }

    return gulp.src(config.paths.src.pluginsScssFiles)
      .pipe(sass({
        sourceComments: config.phase !== 'build',
        includePaths: includePaths,
        quietDeps: true,
        quiet: true,
      }).on('error', sass.logError))
      .pipe(autoprefixer({
        overrideBrowserslist: ['last 2 versions'],
        cascade: false
      }))
      .pipe(prettify({ indent_size: 2 })) 
      .pipe(gulp.dest(config.paths.dist.pluginsCssDir))
      .on('data', function(file) {
        logger.info(`Plugin CSS compiled: ${file.relative}`);
      })
      .pipe(cssmin().on('error', function(err) {
        console.log(err);
      }))
      .pipe(rename({ suffix: '.min' }))    
      .pipe(gulp.dest(config.paths.dist.pluginsCssDir))
      .on('data', function(file) {
        logger.info(`Plugin CSS minified: ${file.relative}`);
      })
      .on('end', done);
  }).catch(err => {
    console.error('Error loading autoprefixer:', err);
    done(err);
  });
});
