'use strict';

const gulp = require('gulp');
const { logger } = require('./utils');

gulp.task('copy-assets', function() {
  const config = global.config || require('./config.js');

  logger.info(`Copying assets from ${config.paths.src.assetsDir} to ${config.paths.dist.assetsDir}, excluding images`);

  return gulp.src([`${config.paths.src.assetsDir}/**/*`])
    .pipe(gulp.dest(config.paths.dist.assetsDir))
    .on('data', function(file) {
      logger.debug(`Copied asset: ${file.relative}`);
    });
});
