'use strict';

const gulp = require('gulp');
const gulpIgnore = require('gulp-ignore');
const cssmin = require('gulp-cssmin');
const rename = require('gulp-rename');
const sass = require('gulp-sass')(require('sass'));
const prettify = require('gulp-jsbeautifier');
const sourcemaps = require('gulp-sourcemaps');
const path = require('path');
const { logger } = require('./utils');
const browserSync = require('browser-sync').create(); // Include BrowserSync
const through = require('through2');

gulp.task('compile-scss', function(done) {
  const config = global.config || require('./config.js');

  // Load autoprefixer asynchronously
  import('gulp-autoprefixer').then(({ default: autoprefixer }) => {

    logger.info(`SCSS files within ${config.paths.src.scssDir} will be compiled and copied to ${config.paths.dist.cssDir}`);

    // Generate CSS variables from config.settings.cssVariables
    // pageSettings.cssVariables are need at page level in compile-html
    let cssVariables = '';
    if (config.settings.cssVariables && Object.keys(config.settings.cssVariables).length > 0) {
      cssVariables = ':root {\n';
      for (const [key, value] of Object.entries(config.settings.cssVariables)) {
        cssVariables += `  ${key}: ${value};\n`;
      }
      cssVariables += '}\n';
      logger.info(`CSS variables added from config: ${cssVariables}`);
    }

    // Custom stream to prepend CSS variables to the SCSS files
    const addCssVariables = () => {
      return through.obj(function(file, enc, cb) {
        const contents = file.contents.toString() + cssVariables;
        file.contents = Buffer.from(contents);
        this.push(file);
        cb();
      });
    };

    let scssStream = gulp.src(config.paths.src.scssFiles)
      .pipe(gulpIgnore.exclude(['_*.scss']))
      .pipe(addCssVariables()); // Prepend the CSS variables

    // Conditionally apply sourcemaps for non-build phases
    if (config.phase !== 'build') {
      scssStream = scssStream.pipe(sourcemaps.init());
    }

    // Include paths
    let includePaths = [
      path.resolve(__dirname, '../node_modules'),
    ];
    
    // Merge additional include paths if paths.src.scssIncludePaths is set
    if (Array.isArray(config.paths.src.scssIncludePaths)) {
      includePaths = includePaths.concat(config.paths.src.scssIncludePaths);
      logger.info(`Merging custom SCSS include paths: ${includePaths.join(', ')}`);
    }

    scssStream = scssStream
      .pipe(sass({
        sourceComments: config.phase !== 'build',
        includePaths: includePaths,
        quietDeps: true,
        quiet: true,
      }).on('error', sass.logError))
      .pipe(autoprefixer({
        overrideBrowserslist: ['last 2 versions'],
        cascade: false
      }))
      .pipe(prettify({ indent_size: 2 }))
      .pipe(gulp.dest(config.paths.dist.cssDir))
      .pipe(browserSync.stream()); // Inject CSS changes without a full reload

    // Conditionally write sourcemaps for development phase
    if (config.phase !== 'build') {
      scssStream = scssStream.pipe(sourcemaps.write('.'));
    }

    // Process minification and saving
    scssStream = scssStream
      .pipe(gulpIgnore.exclude('**/*.map')) // Exclude .map files before minification
      .pipe(cssmin())
      .pipe(rename({ suffix: '.min' }))
      .pipe(gulp.dest(config.paths.dist.cssDir))
      .on('data', function(file) {
        logger.info(`CSS compiled: ${file.relative}`);
      })
      .on('end', done); // Signal task completion with the done callback
  });
});
