'use strict';

const fs = require('fs');
const path = require('path');
const yargs = require('yargs');

// Utility function to handle notification errors in Gulp tasks
function handleNotificationError(err) {
  logger.error('Error in notifier: ', err.message);
}

// Utility function to log errors in Gulp tasks
function logTaskError(taskName) {
  return function(err) {
    logger.error(`Error in ${taskName} task: ${err.message}`);
    handleNotificationError(err);
  };
}

// Variable to track if the initial debug message has been shown
let debugMessageShown = false;

// Logger utility for different log levels
const logger = {
  info(message) {
    console.log(`INFO: ${message}`);
  },

  error(message) {
    console.error(`ERROR: ${message}`);
  },

  debug(message) {
    const config = global.config || require('./config.js'); // Ensure config is loaded
    if (config.settings && config.settings.debugGulp) {
      console.log(`DEBUG: ${message}`);
    }
  }
};

// Reload config and tasks

function reloadConfig(done) {
  // Get the project name from yargs or default to null
  const argv = yargs.option('project', {
    describe: 'Project name for loading specific config',
    type: 'string',
    demandOption: false
  }).argv;
  
  const projectName = argv.project || null;

  // Adjust the paths because utils.js is in the ./gulp folder
  const basePath = path.resolve(__dirname, '..'); // Go one level up to the base directory

  // Clear the require cache for config.default.js
  delete require.cache[require.resolve(path.join(basePath, './src/config.default.js'))];

  // Conditionally clear the cache for project-specific config if it exists
  if (projectName) {
    const projectConfigPath = path.join(basePath, `./projects/${projectName}/config.${projectName}.js`);
    if (fs.existsSync(projectConfigPath)) {
      delete require.cache[require.resolve(projectConfigPath)];
    } else {
      logger.error(`Project config not found for: ${projectName}`);
    }
  }

  // Clear the cache for config.custom.js if it exists
  const customConfigPath = path.join(basePath, './config.custom.js');
  if (fs.existsSync(customConfigPath)) {
    delete require.cache[require.resolve(customConfigPath)];
  }

  // Clear the cache for config.js
  delete require.cache[require.resolve(path.join(basePath, './gulp/config.js'))];

  // Reload the configuration and assign it to global.config
  const defaultConfig = require(path.join(basePath, './src/config.default.js'));
  let projectConfig = {};
  let customConfig = {};

  // Load project-specific config if provided and exists
  if (projectName) {
    const projectConfigPath = path.join(basePath, `./projects/${projectName}/config.${projectName}.js`);
    if (fs.existsSync(projectConfigPath)) {
      projectConfig = require(projectConfigPath);
      logger.info(`Loaded project-specific config for: ${projectName}`);
    }
  }

  // Load custom config if it exists
  if (fs.existsSync(customConfigPath)) {
    customConfig = require(customConfigPath);
  }

  // Merge default, project-specific, and custom configs
  global.config = {
    ...defaultConfig,
    ...projectConfig,
    ...customConfig
  };

  logger.debug(`Settings reloaded: ${JSON.stringify(global.config.settings)}`, global.config); // Log reloaded settings
  done();
}

module.exports = {
  handleNotificationError,
  logTaskError,
  logger,
  reloadConfig
};
