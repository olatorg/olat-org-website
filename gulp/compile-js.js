'use strict';

const gulp = require('gulp');
const { logger } = require('./utils');
const concat = require('gulp-concat');
const jshint = require('gulp-jshint');
const uglify = require('gulp-terser');
const rename = require('gulp-rename');
const merge = require('merge-stream'); // Merge multiple streams

gulp.task('compile-js', function() {
  const config = global.config || require('./config.js');

  logger.info(`JS files within ${config.paths.src.jsDir} will be compiled and copied to ${config.paths.dist.jsDir}`);

  // Core Javascript files
  const coreJsStream = gulp.src(config.paths.src.JSSourceFiles)
    .pipe(concat('script.js'))
    .pipe(jshint())
    .pipe(jshint.reporter('default'))
    .pipe(gulp.dest(config.paths.dist.jsDir))
    .on('data', function(file) {
      logger.info(`Script JS concat done: ${config.paths.dist.jsDir}/${file.relative}`);
    })
    .pipe(uglify())
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest(config.paths.dist.jsDir))
    .on('data', function(file) {
      logger.info(`Script JS minified: ${config.paths.dist.jsDir}/${file.relative}`);
    });

  // Vendor Javascript files
  const vendorJsStream = gulp.src(config.paths.src.JSSourceFilesVendors)
    .pipe(concat('vendors.js'))
    .pipe(gulp.dest(config.paths.dist.jsDir))
    .on('data', function(file) {
      logger.info(`Vendor JS concat done: ${config.paths.dist.jsDir}/${file.relative}`);
    })
    .pipe(uglify())
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest(config.paths.dist.jsDir))
    .on('data', function(file) {
      logger.info(`Vendors JS minified: ${config.paths.dist.jsDir}/${file.relative}`);
    });

  // Custom Javascript files
  const customJsStream = gulp.src(config.paths.src.JSSourceFilesCustom)
    .pipe(concat('custom.script.js'))
    .pipe(jshint())
    .pipe(jshint.reporter('default'))
    .pipe(gulp.dest(config.paths.dist.jsDir))
    .on('data', function(file) {
      logger.info(`Custom JS concat done: ${config.paths.dist.jsDir}/${file.relative}`);
    })
    .pipe(uglify())
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest(config.paths.dist.jsDir))
    .on('data', function(file) {
      logger.info(`Custom JS minified: ${config.paths.dist.jsDir}/${file.relative}`);
    });

  // Return merged streams to ensure Gulp knows when they are done
  return merge(coreJsStream, vendorJsStream, customJsStream);
});
