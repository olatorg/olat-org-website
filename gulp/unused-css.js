'use strict';

const gulp = require('gulp');
const purgecss = require('gulp-purgecss');
const { logger } = require('./utils');

gulp.task('unused-css', function() {
  const config = global.config || require('./config.js');

  // Include both HTML and JavaScript files
  const contentFiles = [
    config.paths.dist.htmlDir + '/**/*.html',  // HTML files
    config.paths.dist.jsDir + '/**/*.js',      // JavaScript files
  ];

  return gulp.src(config.paths.dist.cssDir + '/*.css')
    .on('data', function(file) {
      logger.debug(`Processing Unused CSS for file: ${file.relative}`); // Log each file being processed
    })
    .pipe(purgecss({
      content: contentFiles,  // Include JS files to prevent removal of dynamic classes
      safelist: {
        greedy: [/^data-bs-.*/, /\.show$/, /^data-popper-.*/], // Safeguard all `data-bs-*` attributes and `.show` class
      },
    }))
    .pipe(gulp.dest(config.paths.dist.cssDir))
    .on('data', function(file) {
      logger.debug(`Purged CSS file: ${file.relative}`); // Log each file after purging
    });
});
