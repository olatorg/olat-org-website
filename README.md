# olatorg.gitlab.io

Website for [OLAT-Org](https://gitlab.com/olatorg).

## Usage

1. Clone the source code.

   ```shell
   git clone git@gitlab.com:olatorg/olatorg.gitlab.io.git
   cd olatorg.gitlab.io
   ```

2. Make sure [Node](https://www.nodejs.org/) is installed on your system.

3. Install required packages.

   ```shell
   npm install
   ```

4. Build website locally

   ```shell
   gulp --project=olatorg.gitlab.io
   ````
