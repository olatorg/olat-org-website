'use strict';

/**
 * This is a custom configuration file for Gulp.
 *
 * You can use this file to override the default configuration settings defined in `gulp.config.js`.
 * 
 * How to use:
 * 1. Rename this file to `config.custom.js` to activate the custom configuration.
 * 2. Define only the settings you want to override. The rest will inherit from `gulp.config.js`.
 * 
 * Example:
 * If you want to change the source directory for SCSS files and the output directory for CSS files,
 * you can override just those paths in this file.
 * 
 * Note:
 * - Do not include the entire configuration object. Only include the settings you want to customize.
 * - The default configuration will still be applied for any settings not defined in this custom file.
 */

// Example of overriding the SCSS and CSS paths:
module.exports = {
  paths: {
    scssDir: 'custom/scss',         // Custom source directory for SCSS files
    targetCSSDir: 'custom/css'       // Custom output directory for compiled CSS files
  }
};
