'use strict';

const gulp = require('gulp');
const fs = require('fs-extra');
const path = require('path');
const { logger } = require('./gulp/utils');

// Function to load configuration
function loadConfig(done) {
  delete require.cache[require.resolve('./gulp/config.js')];
  global.config = require('./gulp/config.js');
  done();
}

// Function to load all task files from the 'gulp' directory
function loadTasks(directory) {
  fs.readdirSync(directory).forEach(file => {
    const fullPath = path.join(directory, file);
    if (fs.statSync(fullPath).isDirectory()) {
      loadTasks(fullPath);
    } else if (file.endsWith('.js')) {
      require(fullPath);
    }
  });
}

// Task to clean the build directory using fs-extra
gulp.task('clean-dist', function(done) {
  const config = global.config || require('./config.js');

  // Remove the build directory
  fs.remove(config.paths.dist.base, err => {
    if (err) {
      logger.error(`Error removing dist directory: ${err.message}`);
    } else {
      logger.info(`Successfully removed dist directory: ${config.paths.dist.base}`);
    }
    done();
  });
});

// Load all tasks from the 'gulp' directory
loadTasks(path.join(__dirname, 'gulp'));

// Default task for development
gulp.task('default', gulp.series(
  loadConfig,
  function setDevelopmentPhase(done) {
    global.phase = 'development';
    done();
  },
  'clean-dist',
  gulp.parallel(
    'compile-html', 
    'compile-js', 
    'compile-scss', 
    'compile-plugins-css', 
    'copy-assets', 
  ),
  'assets-map',
  'prettify', 
  'watch'
));

// Makes a production ready build in /dist
// Dynamically load tasks based on config settings
function loadBuildTasks() {
  const config = global.config || require('./gulp/config.js');
  const tasks = [];

  // Conditionally add extra tasks based on buildSettings
  if (config.buildSettings.unusedAssets) {
    tasks.push('unused-assets');
  }
  if (config.buildSettings.unusedCss) {
    tasks.push('unused-css');
  }
  if (config.buildSettings.launchServer) {
    tasks.push('browserSyncServe');
  }

  // @todo: Add critical CSS when implemented
  if (config.buildSettings.criticalCss) {
    // tasks.push('compile-critical-css');
  }

  return gulp.series(
    loadConfig,
    function setDevelopmentPhase(done) {
      global.phase = 'build';
      done();
    },
    'clean-dist',
    gulp.parallel(
      'compile-html', 
      'compile-js', 
      'compile-scss', 
      'compile-plugins-css', 
      'copy-assets', 
    ),
    'assets-map',
    'prettify', 
    ...tasks,
    'compress-imgs',
  );
}

// Define the build task
gulp.task('build', loadBuildTasks());