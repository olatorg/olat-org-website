----------------------------
SOURCE DIRECTORY (/src)
----------------------------

This directory contains the source files for the project.

You are free to edit and alter anything within the following directories:
1. /assets
2. /custom
3. /html
4. /js
5. /scss

/js and /scss import SCSS and Javascript source files from the parent /src directory so you along side the config.PROJECT.js file you can customise the builds to suite your project needs.