// Object of plugins to add to Globals.PLUGINS
Globals.PLUGINS.themeLoadFonts = function (context) {
  //window.addEventListener('load', function() {
  // ----------------------------------------------------------------
  // Loads icon fonts on demand
  // ----------------------------------------------------------------
  var $iconFonts = context.find("[class^=bi-], [class^=ion-], .flag-icon, .la, .lnr, .fa, .fa-solid, .fa-brands");
  var iconFontsToLoad = [];
  if ($iconFonts.length > 0) {
    context.find("body").addClass("icon-fonts-loading");

    if (context.find("[class^=bi-]").length > 0) {
      iconFontsToLoad.push("https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.css");
    }
    if (context.find("[class^=ion-]").length > 0) {
      iconFontsToLoad.push("https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css");
    }
    if (context.find(".flag-icon").length > 0) {
      iconFontsToLoad.push("flag-icon-css/css/flag-icon.min.css");
    }
    if (context.find(".la").length > 0) {
      iconFontsToLoad.push("https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css");
    }
    if (context.find(".lnr").length > 0) {
      iconFontsToLoad.push("https://cdn.linearicons.com/free/1.0.0/icon-font.min.css");
    }
    if (context.find(".fa").length > 0 || context.find(".fa-solid").length > 0 || context.find(".fa-brands").length > 0) {
      iconFontsToLoad.push("https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.6.0/css/all.min.css");
    }

    var iconFontsLoaded = function () {
      context.find("body").removeClass("icon-fonts-loading").addClass("icon-fonts-loaded");
    };

    if (iconFontsToLoad.length > 0) {
      $document.themeLoadPlugin([], iconFontsToLoad, iconFontsLoaded);
    }
  }

  // ----------------------------------------------------------------
  // Loads Google fonts on demand
  // ----------------------------------------------------------------
  var $textFonts = context.find(".text-font-alt, .text-font-slab");
  var textFontsToLoad = [];

  // Check for additional fonts in separate divs
  context.find("[data-additional-fonts]").each(function () {
    var fontUrl = $(this).data("additional-fonts");
    if (fontUrl) {
      textFontsToLoad.push(fontUrl);
    }
  });

  // Check for specific font classes
  if ($textFonts.length > 0) {
    if (context.find(".text-font-alt").length > 0) {
      textFontsToLoad.push("https://fonts.googleapis.com/css2?family=Roboto:wght@100;400;700&display=swap");
    }
    if (context.find(".text-font-slab").length > 0) {
      textFontsToLoad.push("https://fonts.googleapis.com/css2?family=Roboto+Slab:wght@400;700&display=swap");
    }
  }

  // Callback when fonts are loaded
  var textFontsLoaded = function () {
    context.find("body").addClass("text-fonts-loaded");
  };

  // Load the fonts if there are any to load
  if (textFontsToLoad.length > 0) {
    $("head").prepend('<link rel="preconnect" href="https://fonts.googleapis.com">');
    $("head").prepend('<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin></link>');

    $document.themeLoadPlugin([], textFontsToLoad, textFontsLoaded);
  }
  //});
};
