// Object of plugins to add to Globals.PLUGINS
Globals.PLUGINS.themePluginColourSwitcher = function (context) {
  // ----------------------------------------------------------------
  // colour switch - demo only
  // ----------------------------------------------------------------
  var defaultColour = $("html").data("bs-theme") || "theme-green"; // Updated to match `data-bs-theme`
  var colourSchemes = context.find(".theme-colours a");

  // Remove the active class from all schemes and set the active one based on defaultColour
  colourSchemes.removeClass("active");
  colourSchemes.filter("." + defaultColour.replace("theme-", "")).addClass("active"); // Adjusted to handle prefix

  colourSchemes.on("click", function (e) {
    e.preventDefault(); // Prevent default anchor behavior
    var $this = $(this);
    var colorKey = $this.attr("href").replace("#", ""); // Get the color without the # symbol

    // Remove active class from all and add to the clicked one
    $(".theme-colours a").removeClass("active");
    $(".theme-colours a." + colorKey).addClass("active");

    // Update the data-bs-theme attribute on the HTML element
    var themeClass = "theme-" + colorKey; // Prefix with 'theme-'
    $("html").attr("data-bs-theme", themeClass);

    // Set the new default color as active
    defaultColour = themeClass;
  });
};
