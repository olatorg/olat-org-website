// ----------------------------------------------------------------
// Custom navbar-side functionality
// using Bootstrap Offcanvas for heavy lifting
// ----------------------------------------------------------------
var navbarSidePush = document.querySelectorAll(".navbar-side-push, .navbar-side-push-start, .navbar-side-push-end") || false;
var body = document.querySelector("body");
var pushedClass = "navbar-side-pushed";
var pushBreakpoint = "992"; // -lg breakpoint - pushing won't work on small screens
var pushMaxWidth;
var mobileBreakpoint = "576";

var toggleNavbarSidePush = function (el, state, targetWidth) {
  var vw = windowWidth();
  var side = "start";
  if (el.classList.contains("offcanvas-end")) {
    side = "end";
  }
  var margin = side == "start" ? "margin-left" : "margin-right";

  if (pushMaxWidth && targetWidth > pushMaxWidth) {
    targetWidth = pushMaxWidth;
  }

  if (state == "show" && vw >= pushBreakpoint) {
    el.classList.add(pushedClass, pushedClass + "-" + side);
    el.style[margin] = targetWidth + "px";
    body.classList.add("overflow-x-hidden", "navbar-side-open");
  }
  if (state == "hide") {
    el.classList.remove(pushedClass, pushedClass + "-" + side);
    el.style[margin] = targetWidth;
    body.classList.remove("overflow-x-hidden", "navbar-side-open");
  }
};

var toggleNavbarResize = function () {
  var navbarSidePushed = document.querySelectorAll("." + pushedClass) || false;
  if (navbarSidePushed) {
    navbarSidePushed.forEach(function (pushed) {
      var controller = pushed.getAttribute("data-bs-controller") || false;
      var controllerEl;
      if (controller) {
        controllerEl = document.querySelector(controller);
      }
      if (controllerEl) {
        var side = "start";
        if (pushed.classList.contains("offcanvas-end")) {
          side = "end";
        }
        var margin = side == "start" ? "margin-left" : "margin-right";
        pushed.style[margin] = controllerEl.offsetWidth + "px";
      }
    });
  }
};

var offcanvasNavbarTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="navbar-offcanvas"]'));
var offcanvasNavbarList = offcanvasNavbarTriggerList.map(function (offcanvasNavbarEl) {
  var target = offcanvasNavbarEl.getAttribute("data-bs-target") || offcanvasNavbarEl.getAttribute("href");
  var open = offcanvasNavbarEl.getAttribute("data-bs-open") === "true";
  var oc;

  if (target) {
    var targetEl = document.querySelector(target);
    if (targetEl) {
      oc = new bootstrap.Offcanvas(targetEl, {
        backdrop: false,
        scroll: true
      });

      addEvent(offcanvasNavbarEl, "click", function () {
        oc.toggle();
      });

      // Close offcanvas from elements inside targetEl with data-bs-dismiss="offcanvas"
      targetEl.querySelectorAll('[data-bs-dismiss="offcanvas"]').forEach(function (dismissEl) {
        dismissEl.addEventListener("click", function () {
          oc.hide();
        });
      });

      // Close offcanvas from any element with data-bs-dismiss="offcanvas" and matching data-bs-target="TARGET"
      document.querySelectorAll('[data-bs-dismiss="offcanvas"]').forEach(function (dismissEl) {
        var dismissTarget = dismissEl.getAttribute("data-bs-target");
        if (dismissTarget === target) {
          dismissEl.addEventListener("click", function () {
            oc.hide();
          });
        }
      });

      var targetSide = "start",
        targetWidth = targetEl.offsetWidth;
      if (targetEl.classList.contains("offcanvas-end")) {
        targetSide = "end";
      }
      var targetID = targetEl.getAttribute("id") ? "#" + targetEl.getAttribute("id") : false;

      if (targetEl.classList.contains("navbar-side-icon-bar")) {
        pushBreakpoint = 0;
        pushMaxWidth = 80;
      }

      targetEl.addEventListener("show.bs.offcanvas", function () {
        if (navbarSidePush) {
          navbarSidePush.forEach(function (push) {
            var controlledBy = push.getAttribute("data-bs-controller") || false;
            if (targetID && controlledBy && targetID == controlledBy) {
              targetWidth = targetEl.offsetWidth;
              toggleNavbarSidePush(push, "show", targetWidth);
            }
          });
        }
      });
      targetEl.addEventListener("hide.bs.offcanvas", function () {
        if (navbarSidePush) {
          navbarSidePush.forEach(function (push) {
            var controlledBy = push.getAttribute("data-bs-controller") || false;
            if (targetID && controlledBy && targetID == controlledBy) {
              toggleNavbarSidePush(push, "hide", 0);
            }
          });
        }
      });

      // Open by default
      if (open !== false) {
        oc.show();
      }

      // Close Offcanvas on mobile so it works like a toggle
      var vw = windowWidth();
      if (vw <= mobileBreakpoint) {
        setTimeout(function (event) {
          oc.hide();
        }, 500);
      }
      window.addEventListener("resize", function () {
        var vw = windowWidth();
        if (vw <= mobileBreakpoint) {
          oc.hide();
        }
      });
    }
  }
  return oc;
});

window.addEventListener(
  "resize",
  function () {
    setTimeout(function (event) {
      toggleNavbarResize();
    }, 400);
  },
  true
);
