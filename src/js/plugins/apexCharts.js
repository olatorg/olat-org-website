Globals.PLUGINS.themePluginApexCharts = function (context) {
  var $apexcharts = context.find("[data-apexcharts]");

  if ($apexcharts.length > 0) {
    var themePluginApexChartsInit = function () {
      $apexcharts.each(function () {
        var $this = $(this);

        // Get JSON from data-options attribute
        var chartOptions = JSON.parse($this.attr("data-options"));

        // Create the chart
        var chart = new ApexCharts($this[0], chartOptions);

        // Render the chart and trigger resize after it is loaded
        chart.render().then(function () {
          // Trigger a window resize event to ensure proper chart sizing
          window.dispatchEvent(new Event("resize"));
        });
      });
    };

    // Load ApexCharts script and initialize charts after loading
    $document.themeLoadPlugin(["https://cdn.jsdelivr.net/npm/apexcharts"], [], themePluginApexChartsInit);
  }
};
