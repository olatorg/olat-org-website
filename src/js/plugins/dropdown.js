// Object of plugins to add to Globals.PLUGINS
Globals.PLUGINS.themePluginDropdown = function (context) {
  // ----------------------------------------------------------------
  // Bootstrap Click and Hover Dropdown with Edge Detection and Support for Mega Menus
  // ----------------------------------------------------------------

  // Handle hover-triggered dropdowns
  document.querySelectorAll('[data-bs-hover="dropdown"]').forEach(function (dropdownToggle) {
    let dd = dropdownToggle.closest(".dropdown");
    if (dd) {
      // Mouseover event to open dropdown
      dd.addEventListener("mouseover", function () {
        openDropdown(dd, dropdownToggle, true); // Apply hover classes
      });

      // Mouseleave event to close dropdown
      dd.addEventListener("mouseleave", function () {
        closeDropdown(dd, dropdownToggle, true); // Remove hover classes
      });
    }
  });

  // Handle standard Bootstrap dropdowns (click)
  document.querySelectorAll('[data-bs-toggle="dropdown"]').forEach(function (dropdownToggle) {
    let dd = dropdownToggle.closest(".dropdown");
    if (dd) {
      // Use Bootstrap's event listeners for dropdowns
      dd.addEventListener("show.bs.dropdown", function (event) {
        openDropdown(dd, dropdownToggle); // Apply common logic for dropdown opening
      });

      dd.addEventListener("hide.bs.dropdown", function (event) {
        closeDropdown(dd, dropdownToggle); // Apply common logic for dropdown closing
      });
    }
  });

  // Open dropdown and apply necessary classes/adjustments
  function openDropdown(dd, dropdownToggle, isHover = false) {
    let nextEl = dropdownToggle.nextElementSibling;
    if (nextEl) {
      nextEl.classList.add("dropdown-menu-transition");

      if (isHover) {
        dropdownToggle.classList.add("dropdown-hovered");
      }

      // Apply the opened target classes if specified
      let openedClasses = dropdownToggle.getAttribute("data-opened-trigger-class");
      if (openedClasses) {
        openedClasses.split(" ").forEach(function (className) {
          dropdownToggle.classList.add(className);
        });
      }

      // Adjust dropdown position based on type
      if (dd.classList.contains("dropdown-mega-menu")) {
        alignMegaMenu(nextEl, dropdownToggle);
      } else {
        adjustDropdownPosition(nextEl);
      }

      setTimeout(function () {
        if (isHover) {
          nextEl.classList.add("dropdown-hovered");
        }
        nextEl.classList.remove("dropdown-menu-transition");
      }, 10);
    }
  }

  // Close dropdown and remove applied classes
  function closeDropdown(dd, dropdownToggle, isHover = false) {
    let nextEl = dropdownToggle.nextElementSibling;
    if (nextEl) {
      nextEl.classList.add("dropdown-menu-transition");

      if (isHover) {
        dropdownToggle.classList.remove("dropdown-hovered");
        // Delay removal to allow proper transition
        setTimeout(() => {
          nextEl.classList.remove("dropdown-hovered");
        }, 200); // Adjust this delay as needed for smooth transitions
      } else {
        nextEl.classList.remove("dropdown-hovered");
      }

      // Remove the opened target classes if specified
      let openedClasses = dropdownToggle.getAttribute("data-opened-trigger-class");
      if (openedClasses) {
        openedClasses.split(" ").forEach(function (className) {
          dropdownToggle.classList.remove(className);
        });
      }

      nextEl.addEventListener(
        "transitionend",
        function () {
          nextEl.classList.remove("dropdown-menu-transition");
          if (isHover) {
            //dropdownToggle.classList.remove('show');
            //nextEl.classList.remove('show');
          }
        },
        {
          once: true
        }
      ); // Ensure transitionend event is only handled once
    }
  }

  // Generic dropdown position adjustment for regular dropdowns
  function adjustDropdownPosition(dropdownMenu) {
    const rect = dropdownMenu.getBoundingClientRect();
    const viewportWidth = window.innerWidth;

    // Check if the dropdown menu is overflowing to the right
    if (rect.right > viewportWidth) {
      dropdownMenu.style.left = "auto";
      dropdownMenu.style.right = "0";
      dropdownMenu.style.transform = "translateX(-100%)"; // Move the dropdown to the left
    }
    // Check if the dropdown menu is overflowing to the left
    else if (rect.left < 0) {
      dropdownMenu.style.left = "0";
      dropdownMenu.style.right = "auto";
      dropdownMenu.style.transform = "translateX(0)"; // Reset the transform
    }
  }

  // Adjust the position of mega menus specifically
  function alignMegaMenu(dropdownMenu, dropdownToggle) {
    const rect = dropdownToggle.getBoundingClientRect();
    const viewportWidth = window.innerWidth;

    // Calculate the available space considering margins
    const leftPosition = rect.left - parseInt(window.getComputedStyle(dropdownMenu).marginLeft);
    const rightSpace = viewportWidth - rect.right + parseInt(window.getComputedStyle(dropdownMenu).marginRight);

    if (rightSpace < dropdownMenu.offsetWidth) {
      // Align to the right if there's not enough space on the right side
      dropdownMenu.style.left = "auto";
      dropdownMenu.style.right = "0";
      dropdownMenu.style.transform = "translateX(0)";
    } else if (leftPosition + dropdownMenu.offsetWidth > viewportWidth) {
      // Align to the left if it's overflowing to the right
      dropdownMenu.style.left = `${rect.left}px`;
      dropdownMenu.style.right = "auto";
      dropdownMenu.style.transform = "translateX(0)";
    } else {
      // Default alignment (centered)
      //dropdownMenu.style.left = `${rect.left}px`;
      //dropdownMenu.style.right = 'auto';
      //dropdownMenu.style.transform = 'translateX(0)';
    }
  }
};
