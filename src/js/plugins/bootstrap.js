// Object of plugins to add to Globals.PLUGINS
Globals.PLUGINS.themePluginBootstrap = function (context) {
  // ----------------------------------------------------------------
  // Bootstrap integrations
  // ----------------------------------------------------------------

  // Offcanvas
  // -----------
  var offcanvasElementList = [].slice.call(document.querySelectorAll(".offcanvas"));
  var offcanvasList = offcanvasElementList.map(function (offcanvasEl) {
    return new bootstrap.Offcanvas(offcanvasEl);
  });
  window.addEventListener("resize", function () {
    offcanvasList.map(function (offcanvas) {
      offcanvas.hide();
    });
  });

  // ----------------------------------------------------------------
  // Bootstrap tooltip
  // @see: http://getbootstrap.com/javascript/#tooltips
  // ----------------------------------------------------------------
  // invoke by adding data-bs-toggle="tooltip" to a tags (this makes it validate)
  var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'));
  var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
    return new bootstrap.Tooltip(tooltipTriggerEl);
  });

  // ----------------------------------------------------------------
  // Bootstrap popover
  // @see: http://getbootstrap.com/javascript/#popovers
  // ----------------------------------------------------------------
  // invoke by adding data-bs-toggle="popover" to a tags (this makes it validate)
  if ($document.popover) {
    context.find('[data-bs-toggle="popover"]').popover();
  }

  // ----------------------------------------------------------------
  // Bootstrap modals
  // @see: https://getbootstrap.com/docs/5.1/components/modal/
  // ----------------------------------------------------------------
  if (context.find(".modal").length > 0) {
    var myModalEl = document.querySelector(".modal");
    myModalEl.addEventListener("shown.bs.modal", function (event) {
      var focus = this.getAttribute("data-bs-focus-input") || false;
      if (focus) {
        document.querySelector(focus).focus();
      }
    });
  }

  // Tabs
  var tabEl = context.find('[data-bs-toggle="tab"]');
  if (tabEl.length > 0) {
    tabEl.on("shown.bs.tab", function (event) {
      var $target = $(event.target);
      var $relatedTarget = $(event.relatedTarget);
      var targetActiveClass = $target.data("bs-active-class") || "";
      var relatedTargetActiveClass = $relatedTarget.data("bs-active-class") || "";
      if ($target.length > 0 && targetActiveClass) {
        $target.addClass(targetActiveClass);
      }
      if ($relatedTarget.length > 0 && relatedTargetActiveClass) {
        $relatedTarget.removeClass(relatedTargetActiveClass);
      }
    });
  }
};
