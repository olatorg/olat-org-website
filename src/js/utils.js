// ===============================================================
// @group: Internal worker functions
// ===============================================================

(function ($) {
  $.extend($.fn, {
    // ----------------------------------------------------------------
    // Trigger callback when fakeLoader is loaded
    // ----------------------------------------------------------------
    isPageLoaderDone: function (callback) {
      var $loader = $('[data-bs-toggle="page-loader"]');
      var triggerCallback = function () {
        $("html").addClass(".page-loader-done");

        if (callback && typeof callback === "function") {
          callback();
        }
      };

      if ($loader.length === 0 || $loader.css("display") == "none") {
        triggerCallback();
      }

      var isPageLoaderDoneTimer = setInterval(function () {
        if ($loader.css("display") == "none") {
          Globals.PAGELOADER_DONE = true;
          clearInterval(isPageLoaderDoneTimer);
          triggerCallback();
        }
      }, 500);
    },

    // ----------------------------------------------------------------
    // Plugin: Waypoints
    // @see: http://imakewebthings.com/waypoints/
    // Used as helper for other functions so not called direct
    // ----------------------------------------------------------------
    includeWaypoints: function (callback) {
      if (typeof jQuery.fn.waypoint === "undefined") {
        $document.themeLoadPlugin(["https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.min.js"], []);
        var tries = 0;
        var isWaypointsDoneTimer = setInterval(function () {
          if (typeof jQuery.fn.waypoint === "function") {
            clearInterval(isWaypointsDoneTimer);

            if (callback && typeof callback === "function") {
              callback();
            }
          }
          tries++;

          if (tries > 20) {
            clearInterval(isWaypointsDoneTimer);
            alert("Error: Waypoints plugin could not be loaded");
          }
        }, 500);
      } else {
        if (callback && typeof callback === "function") {
          callback();
        }
      }
    },

    refreshWaypoints: function () {
      if (typeof jQuery.fn.waypoint !== "undefined") {
        Waypoint.refreshAll();
      }
    },

    // ----------------------------------------------------------------
    // Determine if element is in viewport
    // @credit: https://coderwall.com/p/fnvjvg/jquery-test-if-element-is-in-viewport
    // ----------------------------------------------------------------
    elementInView: function (obj) {
      var elementTop = obj.offset().top;
      var elementBottom = elementTop + obj.outerHeight();
      var viewportTop = $(window).scrollTop();
      var viewportBottom = viewportTop + $(window).height();
      return elementBottom > viewportTop && elementTop < viewportBottom;
    },

    // ----------------------------------------------------------------
    // Scroll links
    // Migrated from ScrollSpy due to https://github.com/twbs/bootstrap/issues/36431
    // ----------------------------------------------------------------
    themeScrollMenus: function () {
      var context = $(this);
      var scrollLinks = context.find('[data-bs-toggle="scroll-link"]');
      var $header = context.find('#header [data-bs-toggle="sticky"]') || context.find("#header");
      var $body = $("body");
      var $window = $(window);

      if (scrollLinks.length > 0) {
        var offset = {
          scrollOffset: 60, // Default offset
          getHeaderOffset: function () {
            var _offset = this.scrollOffset;

            if ($header.length > 0) {
              _offset = $header.outerHeight();
            }

            if ($body.hasClass("header-compact-sticky")) {
              _offset -= 35;
            }

            if ($body.data("offset-elements")) {
              _offset = 0;
              $($body.data("offset-elements"), context).each(function (k, item) {
                _offset += $(item).outerHeight();
              });
            }

            this.scrollOffset = _offset;
          }
        };

        var updateActiveLink = function (target) {
          scrollLinks.each(function () {
            var $link = $(this);
            var href = $link.attr("href");
            var customActive = $link.data("active-class") || "active";

            if (href === target) {
              $link.addClass(customActive);
            } else {
              $link.removeClass(customActive);
            }
          });
        };

        var smoothScrollTo = function (target) {
          var targetPosition = target.getBoundingClientRect().top + window.pageYOffset - offset.scrollOffset + 5;

          window.scrollTo({
            top: targetPosition,
            behavior: "smooth" // Enables smooth scrolling
          });
        };

        scrollLinks.on("click", function (e) {
          e.preventDefault(); // Prevent default as early as possible

          var $this = $(this);
          var target = this.getAttribute("href");
          var $target = document.querySelector(target);
          var dismiss = $this.data("bs-dismiss");

          if ($target) {
            offset.getHeaderOffset(); // Recalculate offset before scroll
            smoothScrollTo($target);
            updateActiveLink(target);

            // Handle dismissal of modals or offcanvas
            if (dismiss) {
              if (dismiss === "modal") {
                var myModalEl = document.querySelector(".modal.show");
                var modal = bootstrap.Modal.getInstance(myModalEl);
                if (modal) {
                  modal.hide();
                }
              } else if (dismiss === "offcanvas") {
                var myOffcanvasEl = document.querySelector(".offcanvas.show");
                var offcanvas = bootstrap.Offcanvas.getInstance(myOffcanvasEl);
                if (offcanvas) {
                  offcanvas.hide();
                }
              }
            }
          }
        });

        // Update active link on scroll
        $window.on("scroll", function () {
          var scrollPos = $(document).scrollTop() + offset.scrollOffset;

          scrollLinks.each(function () {
            var $link = $(this);
            var href = $link.attr("href");

            // Validate that href exists and it is a valid hash (starts with '#')
            if (href && href.startsWith("#") && href.length > 1) {
              var $target = $(href);

              // Validate that the target element exists in the DOM
              if ($target.length > 0) {
                var targetTop = $target.offset().top;
                var targetBottom = targetTop + $target.outerHeight();

                // Ensure the link becomes active when the section is in view
                if (scrollPos >= targetTop - 50 && scrollPos < targetBottom - 50) {
                  // Adjusted buffer zone
                  updateActiveLink(href);
                }
              }
            }
          });
        });

        // Initial setup
        offset.getHeaderOffset();
        $window.trigger("scroll"); // Trigger scroll to set active link on load
      }
    },

    //submenu dropdowns
    // --------------------------------
    themeSubMenus: function () {
      var context = $(this);
      var $tabsPills = $('.dropdown-menu [data-bs-toggle="tab"], .dropdown-menu [data-bs-toggle="pill"]');

      $tabsPills.on("click", function (e) {
        e.preventDefault();
        e.stopPropagation();
        $(this).tab("show");
      });
      $tabsPills.on("shown.bs.tab", function (e) {
        var $from = $(e.relatedTarget);
        var $to = $(e.target);
        var toSelectors = $to.getSelector();
        var fromSelectors = $from.getSelector();
        var $toSelectors = $(toSelectors);
        var $fromSelectors = $(fromSelectors);

        $toSelectors.addClass("active");
        $fromSelectors.removeClass("active");
        $(document)
          .find('[data-bs-target="' + toSelectors + '"]')
          .addClass("active");
        $(document)
          .find('[data-bs-target="' + fromSelectors + '"]')
          .removeClass("active");
      });

      context.find(".dropdown-menu [data-bs-toggle=dropdown]").on("click", function (event) {
        event.preventDefault();
        event.stopPropagation();

        // Toggle direct parent
        $(this).parent().toggleClass("show");
      });

      // Persistent menus
      context.find(".dropdown.dropdown-persist").on({
        "shown.bs.dropdown": function () {
          $(this).data("closable", false);
        },
        "hide.bs.dropdown": function (event) {
          temp = $(this).data("closable");
          $(this).data("closable", true);
          return temp;
        }
      });
      context.find(".dropdown.dropdown-persist .dropdown-menu").on({
        click: function (event) {
          $(this).parent(".dropdown.dropdown-persist").data("closable", false);
        }
      });
    },

    // Gets selector from href or data-bs-target
    getSelector: function () {
      var element = $(this);
      var selector = element.data("bs-target");
      if (!selector || selector === "#") {
        selector = element.attr("href") || "";
      }

      try {
        var $selector = $(selector);
        return $selector.length > 0 ? selector : null;
      } catch (error) {
        return null;
      }
    },

    // calculate a height with offset
    calcHeightsOffset: function (height, offset) {
      if (typeof offset == "number") {
        return height - offset;
      } else if (typeof offset == "string" && $(offset).length > 0) {
        $(offset).each(function () {
          height = height - $(offset).height();
        });
      }
      return height;
    },

    // Detected IE & adds body class
    isIE: function () {
      if (document.documentMode || /Edge/.test(navigator.userAgent)) {
        return true;
      }
    },

    // Determines plugin location
    // --------------------------------
    getScriptLocation: function () {
      var location = $("body").data("plugins-localpath") || null;
      if (location) {
        return location;
      }
      return Config.PLUGINS_LOCALPATH;
    },

    // Delays
    // --------------------------------
    delay: function (callback, ms) {
      var timer = 0;
      clearTimeout(timer);
      timer = setTimeout(callback, ms);
    },

    // Hash from string
    // --------------------------------
    hashCode: function (str) {
      var hash = 0;
      for (var i = 0; i < str.length; i++) {
        hash = ~~((hash << 5) - hash + str.charCodeAt(i));
      }
      return hash;
    },

    // Load plugin
    // --------------------------------
    themeLoadPlugin: function (js, css, callback, placement, jquery_needed) {
      // Manually loaded assets
      var manualPlugins = $("body").data("plugins-manual") || null;
      if (manualPlugins !== null) {
        if (callback && typeof callback === "function") {
          callback();
        }
        return;
      }

      if (jquery_needed) {
        $("head").append(
          '<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>'
        );
      }

      var themeLoadPluginPath = function (url) {
        if (url.indexOf("http://") === 0 || url.indexOf("https://") === 0) {
          return url;
        }
        var location = $document.getScriptLocation();
        return location + url;
      };

      $.ajaxPrefilter("script", function (s) {
        s.crossDomain = true;
      });
      if (js.length > 0) {
        var progress = 0;
        var internalCallback = function (url) {
          // Complete
          if (++progress === js.length) {
            $.each(css, function (index, value) {
              if (Globals.LOADED_FILES[value] === value) {
                // Already loaded
                return true;
              }

              // Record file loaded
              Globals.LOADED_FILES[value] = value;
              $("head").prepend('<link href="' + themeLoadPluginPath(value) + '" rel="stylesheet" type="text/css" />');
            });

            if (callback && typeof callback === "function") {
              callback();
            }
          }
        };

        var JSLoaderPointEl = $("[data-js-dynamic-loader]");
        $.each(js, function (index, value) {
          if (Globals.LOADED_FILES[value] === value) {
            // Already loaded
            internalCallback();
            return true;
          }

          // Offline mode
          var hash = window.location.hash;
          if (hash === "#offline" && (value.indexOf("http://") === 0 || value.indexOf("https://")) === 0) {
            console.log("Offline mode: " + value + " loading skipped");
            return;
          }

          // Record file loaded
          Globals.LOADED_FILES[value] = value;
          if (placement === undefined) {
            var options = {
              url: themeLoadPluginPath(value),
              dataType: "script",
              success: internalCallback,
              cache: true
            };
            $.ajax(options);
          } else if (placement === "append") {
            JSLoaderPointEl.after('<script src="' + themeLoadPluginPath(value) + '"></script>');
            internalCallback();
          } else if (placement === "prepend") {
            JSLoaderPointEl.before('<script src="' + themeLoadPluginPath(value) + '"></script>');
            internalCallback();
          } else if (placement === "head") {
            $("head").append('<script src="' + themeLoadPluginPath(value) + '"></script>');
            internalCallback();
          }
        });
      } else if (css.length > 0) {
        // Just CSS
        $.each(css, function (index, value) {
          if (Globals.LOADED_FILES[value] === value) {
            // Already loaded
            return true;
          }

          // Record file loaded
          Globals.LOADED_FILES[value] = value;
          $("head").prepend('<link href="' + themeLoadPluginPath(value) + '" rel="stylesheet" type="text/css" />');
        });

        if (callback && typeof callback === "function") {
          callback();
        }
      }
    }
  });
})(jQuery);
