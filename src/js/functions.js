// ===============================================================
// @group: Custom helper functions
// ===============================================================

// Credit: https://plainjs.com/javascript/events/binding-and-unbinding-of-event-handlers-12/
function addEvent(el, type, handler) {
  if (el.attachEvent) el.attachEvent("on" + type, handler);
  else el.addEventListener(type, handler);
}

function removeEvent(el, type, handler) {
  if (el.detachEvent) el.detachEvent("on" + type, handler);
  else el.removeEventListener(type, handler);
}

var windowWidth = function () {
  return Math.max(document.documentElement.clientWidth || 0, window.innerWidth || 0);
};
