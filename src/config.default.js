'use strict';

module.exports = {
  paths: {
    // Source (src): This is your source code which is compile into ./dist (dist) 
    src: {
      base: './src',
      assetsDir: './src/assets',
      scssDir: './src/scss',
      jsDir: './src/js',
      imgDir: './src/assets/img',
      htmlDir: './src/html',
      htmlPartialsDir: './src/html/@partials',
      pluginsScssFiles: [
        './src/scss/plugins/*.scss',
      ],
      scssFiles: [
        './src/scss/*.scss',
      ],
      scssFilesWatched: [
        './src/scss/*.scss',
        './src/scss/**/*.scss',
      ],
      scssIncludePaths: [
        './src/scss',
      ],
      JSSourceFiles: [
        // Core Javscript files
        './src/js/start.js',
        './src/js/functions.js',
        './src/js/setup.js',
        './src/js/utils.js',
        './src/js/misc.js',
        './src/js/plugins.js',
        './src/js/plugins/*.js',
        './src/js/base.js',
        '!node_modules/**',
        //'!./src/js/plugins/fakeLoader.js', // Example: exclude a given plugin
      ],
      JSSourceFilesVendors: [
        // Required vendor files
        'node_modules/bootstrap/dist/js/bootstrap.bundle.js',
        'node_modules/@popperjs/core/dist/umd/popper.min.js',
        'node_modules/jquery/dist/jquery.js',
      ],
      JSSourceFilesCustom: [
        './src/js/custom.script.js', // Your custom Javascript: you can add files or pass an empty array to remove, also see settings > includeCustomJS
      ]
    },

    // dist (dist): Where you're assets are compiled to in development and on build
    dist: {
      base: './dist',
      assetsDir: './dist/assets',
      cssDir: './dist/assets/css',
      jsDir: './dist/assets/js',
      imgDir: './dist/assets/img',
      htmlDir: './dist',
      pluginsCssDir: './dist/assets/plugins/plugin-css'
    },   
  },

  settings: {
    /**
     * Define your site-wide settings here. These settings can be seamlessly integrated into your HTML templates
     * by referencing them with the {{ variableName }} syntax. Twig, the templating engine used, allows you
     * to dynamically insert content into your HTML by replacing these placeholders with the values defined in this configuration.
     * 
     * Example:
     * If you define settings like:
     *
     *   siteName: 'My Website',
     *   siteSlogan: 'An awesome website'
     *
     * You can use them in your HTML files as:
     *
     *   <title>{{ siteName }}</title>
     *   <h1>{{ siteName }}</h1>
     *   <p>{{ siteSlogan }}</p>
     *
     * This will automatically replace the placeholders with the corresponding values, allowing you to manage
     * site-wide content from a single location.
     * 
     * Additionally, you can add your own custom settings here and reference them within your HTML. 
     * Twig provides powerful features, such as conditional rendering, loops, filters, and more, making it a versatile
     * tool for managing your site’s content. For more information, see the Twig documentation: 
     * https://twig.symfony.com/.
     * 
     * Explore these features and extend your templates with custom settings to create dynamic, maintainable HTML.
     */


    siteName: 'App<span class="text-primary">Strap</span>.',
    siteSlogan: 'Bootstrap 5 Theme (updated)',
    //siteLogo: { // add a logo img to header
      //src: 'assets/img/themelizeme-logo.png',
      //height: '60px',
      //width: '120px',
      //alt: "AppStrap logo",
      //class: 'some-class'
    //},
    siteNameHidden: false, // if you prefer a logo image you can visually hide the siteName
    siteFooterCopyright: 'Site template by <a href="https://themelize.me" class="footer-link">Themelize.me</a> | Copyright 2024 &copy; AppStrap.',
    navbarBrandUrl: 'index.html',
    
    // Present Colour scheme
    // Set one of our preset colour schemes as the default colour scheme
    // To add your own primary colour @see /src/custom/scss/_variables.custom.scss
    colourScheme: 'purple',

    // Features
    headerSticky: true,
    headerSearch: true,
    headerOffcanvas: true,
    pagePreloader: true,

    // Mobile menu
    headerNavbarToggle: true,
    mobileMenuType: 'offcanvas', // or 'collapse'
    mobileMenuTarget: '#navbar-main',

    // Misc. settings
    debugGulp: false,  // Set to true to enable debugging output while running Gulp
    includeColourSwitcher: true, // for demo purposes, disable on prduction
    includeCustomJS: true, // set to false to not include the file assets/js/custom.script.js in HTML pages

    // Assets
    mainCSS: '', // the main CSS file, combine with paths.src.scssFiles to provide your own custom main css file
    mainJs: '', // the main JS file, combine with paths.src.JSSourceFiles to provide your own custom main javascript file
    additionalCSS: [ // add additional CSS files to <head>
      //'https://fonts.googleapis.com/css2?family=Inter:ital,opsz,wght@0,14..32,100..900;1,14..32,100..900&family=SUSE:wght@100..800&display=swap',
      // './projects/myproject/css/custom2.css'
    ],
    additionalJS: [ // add additional JS files to footer
      // './projects/myproject/js/custom1.js',
      // './projects/myproject/js/custom2.js'
    ],
    additionalFonts: [// Load additional Google fonts ondemand after the initial page load
      //'https://fonts.googleapis.com/css2?family=SUSE:wght@100..800&display=swap'
    ],

    // CSS Variable
    // Powerful tool to override Bootstrap CSS variables at build time
    // For more complex changes like primary colours you will be to use /src/custom/scss files
    // cssVariables: {
    //   '--bs-font-sans-serif': '"SUSE", system-ui, -apple-system, "Segoe UI", Roboto, "Helvetica Neue", "Noto Sans", "Liberation Sans", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji"',
    // },

    // Data
    // Powerful tool to inject custom global data into your HTML pages
    data: {
      customerCount: '5500+', // OR ping an API 
    },
  },

  /**
   * Define page-specific settings here. These settings will override the global settings
   * for the specific page they are associated with. The key should be the name of the HTML
   * file (without the .html extension), or you can use wildcards (*) to match multiple pages.
   * Each value should be an object containing the settings specific to that page or group of pages.
   *
   * Options:
   * - **Specific Page Settings**: Define settings for an exact page by using the page name without the `.html` extension.
   * - **Wildcard Matching**: Use `*` to define settings for multiple pages at once. For example, `shop-*` applies to `shop-cart.html`, `shop-checkout.html`, etc.
   * - **Inheritance**: Use the `inheritFrom` option to base the settings on another page's configuration and override only certain values.
   *
   * Example:
   * If you define page-specific settings like this:
   *
   *   pageSettings: {
   *     index: {
   *       headerOffcanvas: false,  // Applies only to index.html
   *       data: {
   *         someDataJustFromThisPage: []
   *       }
   *     },
   *     'shop-*': {  // Applies to any page starting with 'shop-'
   *       headerSticky: false,
   *     },
   *     'shop-cart': {
   *       inheritFrom: 'shop-*',  // Inherits from 'shop-*', but overrides the sticky header
   *       headerSticky: true,
   *     }
   *   }
   *
   * These settings will be used in the corresponding HTML files (e.g., index.html, shop-cart.html, etc.).
   * If a variable is defined in both the global configuration and here, the value defined here will
   * take precedence for that specific page.
   * 
   * pageSettings can also be provided in .html files as JSON front matter at the start of the file:
   * Example:
   * <!-- 
   *  {
   *    "additionalCSS": ["css/custom.css"],
   *    "cssVariables": {
   *     "--bs-primary": "#007bff",
   *      "--bs-secondary": "#6c757d"
   *    },
   *   "headerSticky": true
   *  }
   *  -->
   * HTML CODE HERE
   */
  pageSettings: {
    'shop': {  // Applies to any page starting with 'shop-'
      additionalFonts: [// Load additional Google fonts ondemand after the initial page load
        'https://fonts.googleapis.com/css2?family=SUSE:wght@100..800&display=swap'
      ],
      cssVariables: {
        '--bs-font-sans-serif': '"SUSE", system-ui, -apple-system, "Segoe UI", Roboto, "Helvetica Neue", "Noto Sans", "Liberation Sans", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji"',
      },
    },
    'shop-*': {
      inheritFrom: 'shop'
    },
  },


  // Customise the gulp build command for production ready builds
  buildSettings: {
    launchServer: false,
    unusedCss: false,
    unusedAssets: true,
    criticalCss: true, // @todo: coming soon
  }
};
